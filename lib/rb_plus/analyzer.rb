module RbPlus
  class Analyzer
    attr_reader :learning_data, :regions, :img, :img_name

    def analysis(filename)
      raise ArgumentError "Filename must be String" unless filename.kind_of? String
      @img_name = filename
      @img = IplImage.load(@img_name)
      raise RuntimeError "Image can't load" if img == nil

      response = RbPlus::Response.new
      response.filename = @img_name
      response.difficulty, response.level = detect_difficulty
      response.score  = detect_score(@regions[:score])
      response.achievement_rate = detect_score(@regions[:achievement_rate], is_percent: true)
      response.just = detect_score(@regions[:just])
      response.great = detect_score(@regions[:great])
      response.good = detect_score(@regions[:good])
      response.miss = detect_score(@regions[:miss])
      response.just_reflec = detect_score(@regions[:just_reflec])
      response.max_combo = detect_score(@regions[:max_combo])

      return response
    end

    def detect_difficulty()
      level = nil
      difficulty = nil
      max_score = 0.90 # threshold
      @img.set_roi(@regions[:difficulty])
      @learning_data[:difficulty].each do |data|
        result = @img.match_template(data.img, CV_TM_CCOEFF_NORMED)
        if result.min_max_loc[1] > max_score then
          difficulty = data.val[:difficulty]
          level = data.val[:level]
          max_score = result.min_max_loc[1]
        end
      end

      if level == nil
        @img.save(File.dirname(File.dirname(File.dirname(__FILE__))) + "/failed/" + File.basename(@img_name) + "_difficulty.png")
      end

      @img.reset_roi
      return [difficulty, level]
    end

    def detect_score(region, is_percent: false, threshold: 0.88)
      @img.set_roi(region)
      img_ = @img.clone
      scores = {}
      @learning_data[:num].each do |data|
        while true

          result = img_.match_template(data.img, CV_TM_CCOEFF_NORMED)
          min_score, max_score, min_point, max_point = result.min_max_loc

          if max_score < threshold then
            break
          end

          scores[max_point.x.to_i] = data.val[:num].to_i

          img_.rectangle!(max_point, CvPoint.new(max_point.x + data.img.width, max_point.y + data.img.height), :color => CvColor::Red, :thickness => 1)
          img_.flood_fill!(max_point, CvColor::Red)
        end
      end

      @img.reset_roi

      score_str = "";
      (scores.sort{|(k1, v1), (k2, v2)| k1 <=> k2 }).each do |key, val|
        score_str += val.to_s
      end

      return is_percent ? score_str.to_f / 10 : score_str.to_i
    end

    def initialize()
      # TODO: 引数取れるようにしてもいいかも
      @learning_data = {
        :difficulty => [
          # Basic
          LearningData.new(:difficulty, File.dirname(__FILE__) + '/template/difficulty/basic_3.png', {:difficulty => 'basic', :level => '3',}),
          LearningData.new(:difficulty, File.dirname(__FILE__) + '/template/difficulty/basic_4.png', {:difficulty => 'basic', :level => '4',}),
          # Medium
          LearningData.new(:difficulty, File.dirname(__FILE__) + '/template/difficulty/medium_5.png', {:difficulty => 'medium', :level => '5',}),
          LearningData.new(:difficulty, File.dirname(__FILE__) + '/template/difficulty/medium_6.png', {:difficulty => 'medium', :level => '6',}),
          LearningData.new(:difficulty, File.dirname(__FILE__) + '/template/difficulty/medium_7.png', {:difficulty => 'medium', :level => '7',}),
          # Hard
          LearningData.new(:difficulty, File.dirname(__FILE__) + '/template/difficulty/hard_7.png', {:difficulty => 'hard', :level => '7',}),
          LearningData.new(:difficulty, File.dirname(__FILE__) + '/template/difficulty/hard_8.png', {:difficulty => 'hard', :level => '8',}),
          LearningData.new(:difficulty, File.dirname(__FILE__) + '/template/difficulty/hard_9.png', {:difficulty => 'hard', :level => '9',}),
          LearningData.new(:difficulty, File.dirname(__FILE__) + '/template/difficulty/hard_10.png', {:difficulty => 'hard', :level => '10',}),
          LearningData.new(:difficulty, File.dirname(__FILE__) + '/template/difficulty/hard_11.png', {:difficulty => 'hard', :level => '11',})
        ],
        :num => [
          LearningData.new(:num, File.dirname(__FILE__) + '/template/num/0.png', {:num => 0}),
          LearningData.new(:num, File.dirname(__FILE__) + '/template/num/1.png', {:num => 1}),
          LearningData.new(:num, File.dirname(__FILE__) + '/template/num/2.png', {:num => 2}),
          LearningData.new(:num, File.dirname(__FILE__) + '/template/num/3.png', {:num => 3}),
          LearningData.new(:num, File.dirname(__FILE__) + '/template/num/4.png', {:num => 4}),
          LearningData.new(:num, File.dirname(__FILE__) + '/template/num/5.png', {:num => 5}),
          LearningData.new(:num, File.dirname(__FILE__) + '/template/num/6.png', {:num => 6}),
          LearningData.new(:num, File.dirname(__FILE__) + '/template/num/7.png', {:num => 7}),
          LearningData.new(:num, File.dirname(__FILE__) + '/template/num/8.png', {:num => 8}),
          LearningData.new(:num, File.dirname(__FILE__) + '/template/num/9.png', {:num => 9}),
        ]
      }

      @regions = {
        :title => CvRect.new(77, 59, 286, 40),
        :difficulty => CvRect.new(23, 59, 46, 46),
        :score => CvRect.new(260, 175, 105, 20),
        :achievement_rate => CvRect.new(260, 195, 105, 20),
        :just => CvRect.new(260, 225, 45, 15),
        :great => CvRect.new(260, 240, 45, 17),
        :good => CvRect.new(260, 257, 45, 15),
        :miss => CvRect.new(260, 274, 45, 17),
        :just_reflec => CvRect.new(260, 291, 105, 15),
        :max_combo => CvRect.new(260, 308, 45, 17)
      }
    end
  end
end
