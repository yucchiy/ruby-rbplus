module RbPlus
  class LearningData
    attr_reader :type, :val, :img
    def initialize(type, image_file, val)
      @type = type
      @val = val
      @img = CvMat.load image_file
      raise RuntimeError "Image can't load" if @img == nil
    end
  end
end
