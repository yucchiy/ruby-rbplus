module RbPlus
  class Response
    attr_accessor :filename, :difficulty, :level, :music_name, :score, :achievement_rate, :just, :great, :good, :miss, :just_reflec, :max_combo

    def initialize()
      @difficulty = nil
      @score = nil
    end

    def full_combo?
      return @miss == 0
    end

    def rank
      if @achievement_rate < 60.0
        return "C"
      elsif @achievement_rate < 70.0
        return "B"
      elsif @achievement_rate < 80.0
        return "A"
      elsif @achievement_rate < 90.0
        return "AA"
      elsif @achievement_rate < 95.0
        return "AAA"
      else
        return "AAA+"
      end
    end

    def notes
      return @just + @great + @good + @miss
    end

    def just_percent
      return @just.to_f * 100 / notes
    end

    def great_percent
      return @great.to_f * 100 / notes
    end

    def good_percent
      return @good.to_f * 100 / notes
    end

    def miss_percent
      return @miss.to_f * 100 / notes
    end

    def success?
      return (@difficulty != nil and @score != nil)
    end

  end
end
