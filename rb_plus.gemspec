# coding: utf-8
lib = File.expand_path('../lib', __FILE__)
$LOAD_PATH.unshift(lib) unless $LOAD_PATH.include?(lib)
require 'rb_plus/version'

Gem::Specification.new do |spec|
  spec.name          = "rb_plus"
  spec.version       = RbPlus::VERSION
  spec.authors       = ["Yuichiro Mukai"]
  spec.email         = ["y.iky917@gmail.com"]
  spec.description   = %q{Result Screen Analyzer for Reflec Beat Plus}
  spec.summary       = %q{Result Screen Analyzer for Reflec Beat Plus}
  spec.homepage      = ""
  spec.license       = "MIT"

  spec.files         = `git ls-files`.split($/)
  spec.executables   = spec.files.grep(%r{^bin/}) { |f| File.basename(f) }
  spec.test_files    = spec.files.grep(%r{^(test|spec|features)/})
  spec.require_paths = ["lib"]


  spec.add_development_dependency "bundler", "~> 1.3"
  spec.add_development_dependency "rake"

  # for testing
  spec.add_development_dependency "rspec"

  spec.add_development_dependency "ruby-opencv"
end
