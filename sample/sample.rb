require 'rubygems'
$:.unshift File.expand_path "../lib", File.dirname(__FILE__)
require 'rb_plus'

analyzer = RbPlus::Analyzer.new

Dir::glob(File.dirname(__FILE__) + "/tests/*").each do |img_file|
  result = analyzer.analysis img_file
  p "---- " + result.filename + " ----"
  if result.success?
    p result.difficulty + " " + result.level.to_s
    p result.full_combo? ? "Rank = " + result.rank + " (Full combo!)" : "rank = " + result.rank
    p "Score = Just: " + result.just.to_s + ", Great: " + result.great.to_s + ", Good: " + result.good.to_s + ", Miss: " + result.miss.to_s + ", Just Reflec: " + result.just_reflec.to_s + ", Max combo: " + result.max_combo.to_s
  else
    p "Failed to analysis"
  end

end
